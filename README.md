# Symfony serializer examples
This repository shows basic usage of Symfony serializer component with plain examples with nested objects.

#Routes:
/simpleOrder
- basic example with perfectly formatted JSON

/case1
- if nested object is exspected to be array of objects, but just object is provided in JSON
 
/case2
- if object is expected but null / empty string is provided in JSON

/case3
- if we need to convert JSON to array before we normalize it to objects

In this use cases it is coverd how to solve most usual problems using Symfony serializer component:

Deserialize from JSON : wrong attribute type,
Change serialized property name in Symfony serialisation,
How to Create your Custom Normalizer,
Converting a json request to an array in Symfony
Symfony - Deserialize json to an array of entities,
jsondecode to array symfony.