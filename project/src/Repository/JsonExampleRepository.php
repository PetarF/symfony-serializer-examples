<?php


namespace App\Repository;


class JsonExampleRepository
{
    private $json = [
        'simpleOrder' => '{
            "id":"123",
            "address":{
                "city":"City",
                "street":"street",
                "street_number":"street number"
                },
            "total_price":"123",
            "total_quantity":"2",
            "items":[
                {
                "name":"item One",
                "quantity":"2",
                "price":"123"    
                },
                {
                "name":"item One",
                "quantity":"2",
                "price":"123"
                }
            ]
            }',
        'case1' => '{
            "id":"123",
            "address":{
                "city":"City",
                "street":"street",
                "street_number":"street number"
                },
            "total_price":"123",
            "total_quantity":"2",
            "items":
                {
                "name":"item One",
                "quantity":"2",
                "price":"123"    
                }
            }',

        'case2' => '{
            "id":"123",
            "address":null,
            "total_price":"123",
            "total_quantity":"2",
            "items":""
            }',
    ];

    public function getSimpleOrder(): string
    {
        return $this->json['simpleOrder'];
    }

    public function getCase1(): string
    {
        return $this->json['case1'];
    }

    public function getCase2(): string
    {
        return $this->json['case2'];
    }
}