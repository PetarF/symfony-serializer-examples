<?php

namespace App\Controller;

use App\Entity\Data\Order;
use App\Repository\JsonExampleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @var SerializerInterface
     */
    private $serializer;
    /**
     * @var JsonExampleRepository
     */
    private $jsonExampleRepository;

    /**
     * MainControlller constructor.
     * @param SerializerInterface $serializer
     * @param JsonExampleRepository $jsonExampleRepository
     */
    public function __construct(SerializerInterface $serializer, JsonExampleRepository $jsonExampleRepository)
    {
        $this->serializer = $serializer;
        $this->jsonExampleRepository = $jsonExampleRepository;
    }

    /**
     * @Route ("/simpleOrder", name="simple_order")
     * @return Response
     */
    public function simpleOrder(): Response
    {
        $json = $this->jsonExampleRepository->getSimpleOrder();
        $order = $this->serializer->deserialize($json, Order::class, 'json');

        dd($order);
    }

    /**
     * @Route ("/case1", name="case1")
     * @return Response
     */
    public function case1(): Response
    {
        $json = $this->jsonExampleRepository->getCase1();
        $order = $this->serializer->deserialize($json, Order::class, 'json');

        dd($order);
    }

    /**
     * @Route ("/case2", name="case2")
     * @return Response
     */
    public function case2(): Response
    {
        $json = $this->jsonExampleRepository->getCase2();
        $order = $this->serializer->deserialize($json, Order::class, 'json');

        dd($order);
    }

    /**
     * @Route ("/case3", name="case3")
     * @return Response
     */
    public function case3(): Response
    {
        $json = $this->jsonExampleRepository->getSimpleOrder();
        $data = $this->serializer->decode($json,'json');
        $order = $this->serializer->denormalize($data, Order::class);

        dd($order);
    }

}