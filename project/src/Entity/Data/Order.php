<?php


namespace App\Entity\Data;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Order
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     * @SerializedName("total_price")
     */
    private $totalPrice;

    /**
     * @var string
     * @SerializedName("total_quantity")
     */
    private $totalQuantity;
    /**
     * @var OrderAddress
     */
    private $address;

    /**
     * @var OrderItem[]
     */
    private $items;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTotalPrice(): string
    {
        return $this->totalPrice;
    }

    /**
     * @param string $totalPrice
     */
    public function setTotalPrice(string $totalPrice): void
    {
        $this->totalPrice = $totalPrice;
    }

    /**
     * @return string
     */
    public function getTotalQuantity(): string
    {
        return $this->totalQuantity;
    }

    /**
     * @param string $totalQuantity
     */
    public function setTotalQuantity(string $totalQuantity): void
    {
        $this->totalQuantity = $totalQuantity;
    }

    /**
     * @return OrderAddress
     */
    public function getAddress(): OrderAddress
    {
        return $this->address;
    }

    /**
     * @param OrderAddress $address
     */
    public function setAddress(OrderAddress $address): void
    {
        $this->address = $address;
    }

    /**
     * @return OrderItem[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param OrderItem[] $items
     */
    public function setItems(array $items): void
    {
        $this->items = $items;
    }


}
