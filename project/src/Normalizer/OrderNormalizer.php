<?php


namespace App\Normalizer;


use App\Entity\Data\Order;
use Symfony\Component\Serializer\Exception\BadMethodCallException;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Exception\ExtraAttributesException;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Exception\LogicException;
use Symfony\Component\Serializer\Exception\RuntimeException;
use Symfony\Component\Serializer\Exception\UnexpectedValueException;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class OrderNormalizer implements DenormalizerInterface
{
    /**
     * @var ObjectNormalizer
     */
    private $normalizer;

    /**
     * OrderNormalizer constructor.
     * @param ObjectNormalizer $normalizer
     */
    public function __construct(ObjectNormalizer $normalizer)
    {
        $this->normalizer = $normalizer;
    }


    public function denormalize($data, string $type, string $format = null, array $context = [])
    {
        $data['id'] = (int) $data['id'];
        if(!isset($data['items'][0])){
            $data['items'] = [$data['items']];
        }

        return $this->normalizer->denormalize($data,$type,$format,$context);
    }

    public function supportsDenormalization($data, string $type, string $format = null)
    {
        return $type === Order::class;
    }

}