#!/usr/bin/env sh
#usermod -u 1000 www-data
#chown 1000 /var/www -R

if [ ! -d /var/www/var/ ] 
then
    mkdir -p /var/www/var
    mkdir -p /var/www/var/logs
	chmod -R ugo+rwx /var/www/var
fi 

chown -R www-data:www-data /var/www
find /var/www -type d -exec chmod 755 {} \;
find /var/www -type f -exec chmod 644 {} \;
/usr/local/sbin/php-fpm